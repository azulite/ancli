/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AntestComponent } from './antest.component';

describe('AntestComponent', () => {
  let component: AntestComponent;
  let fixture: ComponentFixture<AntestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AntestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AntestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
