import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { RentalOverdue } from '../model/rental-overdue';
import { TxRecord } from '../model/txRecord';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-antest',
  templateUrl: './antest.component.html',
  styleUrls: ['./antest.component.css']
})
export class AntestComponent implements OnInit {
  param = {value: '4n6ul4r 7357 '};
  flangestname = 'EST_CHI_NAME';
  page = 1;
  txRecords: TxRecord[];
  pageSize = 0;
  model: any;
  tx_model: any;
  searching = false;
  searchFailed = false;
  selectedEst: any;
  relativeEsts: any;

  rentaloverdues: RentalOverdue[];

  constructor( @Inject('filmList') private filmList, private translate: TranslateService, private route: Router) {
    let url1 = this.route.url.split('/');
    console.log('url: ' + url1);
    console.log('url 1: ' + url1[1]);
    if (url1[1] === 'en') {
      translate.setDefaultLang('en');
      translate.use('en');
      this.flangestname = 'EST_ENG_NAME';
    } else {
      translate.setDefaultLang('zh');
      translate.use('zh');
      this.flangestname = 'EST_CHI_NAME';
    }
  }

  ngOnInit() {
    let url1 = this.route.url.split('/');
    if (url1[3]) {
      this.pageChanged(url1[3]);
    } else {
      this.pageChanged();
    }
    
    this.loadRelativeEsts();
  }

  // auto complete section
  search = (text$: Observable<string>) =>
    text$
      .debounceTime(300)
      .distinctUntilChanged()
      .do(() => this.searching = true)
      .switchMap(term =>
        this.filmList.getAutoComplete(term)
          .do(() => this.searchFailed = false)
          .do(items => console.log('auto result: ', items.map((item: Object) => Object.keys(item)[0])))
          .catch(() => {
            this.searchFailed = true;
            return Observable.of([]);
          }))
      .do(() => this.searching = false)

  // reform the api result item into suitable string value
  resultFormatter = (value: any) => Object.keys(value)[0];

  // reform the api result item into suitable string value
  inputFormatter = (value: any) => Object.keys(value)[0];


  // tx section
  // pageSize = new Observable(observer => {
  //   setTimeout(() => {
  //     console.log('pagesize');
  //     observer.next(10000);
  //   }, 1000);
  // })

  selectedItem(item) {
    this.selectedEst = item.item;
    this.page = 1;
    // console.log('selectedItem: ', this.selectedEst);

    this.pageChanged();
    this.loadRelativeEsts();
  }

  pageChanged(targetEstID = 'E12603') {
    //let targetEstID: string;
    if (this.selectedEst) {
      // console.log('this.selectedEst[0]: ',this.selectedEst[0]);
      targetEstID = this.selectedEst[Object.keys(this.selectedEst)[0]];
      // console.log('targetEstID: ', targetEstID);
    }
    // console.log("newPage: ", this.page); 
    this.filmList.getTxRecords(targetEstID.trim(), this.page)
      .subscribe(
      txRecords => this.changePage(txRecords), // this.txRecords = txRecords.tx, // Bind to view
      err => {
        // Log errors if any
        console.log(err);
      });
  }

  private changePage(txRecords) {
    this.txRecords = txRecords.tx;
    this.pageSize = txRecords.txcount;
  }

  loadRelativeEsts(){
    let targetDistID: string;
    if (!this.selectedEst) {
      targetDistID = '1010030000';
      // console.log('targetDistID default: ', targetDistID);
    } else {
      targetDistID = this.selectedEst['DIST_ID'];
      // console.log('targetDistID: ', targetDistID);
    }
    this.filmList.getRelativeEsts(targetDistID.trim())
      .subscribe(
      relativeEsts =>  this.relativeEsts = relativeEsts, // Bind to view
      err => {
        console.log(err);
      });
  }

}
