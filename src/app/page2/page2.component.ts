import { Component, OnInit, Inject, AfterContentInit } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { RentalOverdue } from '../model/rental-overdue';
import { Exscript1 } from '../exscript1.service';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css'],
  providers: [Exscript1]
})
export class Page2Component implements OnInit, AfterContentInit {

  rentaloverdues: RentalOverdue[];
  hreflang = "zh-hk";

  calc2Cols = '2 2 calc(10em + 10px);';   /** 10px is the missing margin of the missing box */
  calc3Cols = '3 3 calc(15em + 20px)';

    constructor(
        @Inject('filmList') private filmList,
        private exscript1: Exscript1
    ) { }

    ngOnInit() {
        // Load comments
        this.loadFilms();
        this.exscript1.testScript1();
    }

    ngAfterContentInit() {

    }

    loadFilms() {
        // Get all comments
        this.filmList.getRentalOverdue()
            .subscribe(
            rentaloverdues => this.rentaloverdues = rentaloverdues, // Bind to view
            err => {
                // Log errors if any
                console.log(err);
            });
    }

}
