// import { type } from 'os';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-print-btn',
  template: `
  <div>
    <span></span>
    <button type="button" name="Print" md-raised-button>Print with cat ID {{cat.category_id}}</button>
  </div>
  `,
  styles: []
})
export class PrintBtnComponent implements OnInit {

  @Input() cat;

  constructor() { }

  ngOnInit() {
  }

}
