import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    @Inject('filmList') private filmList,
    @Inject('estateList') private estateList
  ) { }

  ngOnInit() {
  }

}
