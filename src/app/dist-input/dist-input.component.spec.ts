/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DistInputComponent } from './dist-input.component';

describe('DistInputComponent', () => {
  let component: DistInputComponent;
  let fixture: ComponentFixture<DistInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
