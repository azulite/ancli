import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { RouterModule, Routes, ActivatedRoute, Params } from '@angular/router';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MetaModule, MetaService, MetaConfig } from 'ng2-meta';
import 'hammerjs';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { FilmListComponent } from './film-list/film-list.component';
import { FilmListService } from './film-list.service';
// import { FilmDirectiveDirective } from './film-directive.directive';

import appRoutes from './app.routes';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DistrictBlockComponent } from './district-block/district-block.component';
import { PrintBtnComponent } from './print-btn/print-btn.component';
import { Page2Component } from './page2/page2.component';
import { DistrictSelectionComponent } from './district-selection/district-selection.component';
import { DistInputComponent } from './dist-input/dist-input.component';
import { AntestComponent } from './antest/antest.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const metaConfig: MetaConfig = {
  // Append a title suffix such as a site name to all titles
  // Defaults to false
  useTitleSuffix: true,
  defaults: {
    title: 'Default title for pages without meta in their route',
    titleSuffix: ' | Angular',
    'og:image': 'http://example.com/default-image.png',
    'any other': 'arbitrary tag can be used'
  }
};

export function HttpLoaderFactory(http: Http) {
  // return new TranslateHttpLoader(http, "/public/lang-files/", "-lang.json");
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    FilmListComponent,
    HeaderComponent,
    FooterComponent,
    DistrictBlockComponent,
    PrintBtnComponent,
    Page2Component,
    DistrictSelectionComponent,
    DistInputComponent,
    AntestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot(),
    NgbModule.forRoot(),
    MetaModule.forRoot(metaConfig),
    FlexLayoutModule,
    appRoutes,
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        })
  ],
  // providers: [FilmListService],
  providers: [
    {provide: 'filmList', useClass: FilmListService},
    {provide: 'estateList', useValue: 'http://express.jakeh.io/estates'},
    {provide: 'metaService', useClass: MetaService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
