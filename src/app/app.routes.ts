import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

import { FilmListComponent } from './film-list/film-list.component';
import { HomeComponent } from './home/home.component';
import { Page2Component } from './page2/page2.component';
import { AntestComponent } from './antest/antest.component';

const routes = [
  {
    path: 'filmlist',
    component: FilmListComponent,
    data: {
      meta: {
        title: 'Title - Film List',
        description: 'Angular - Film listing'
      }
    }
  },
  {
    path: 'page2',
    component: Page2Component,
    data: {
      meta: {
        title: 'Title - Page 2',
        description: 'Angular - Page 2'
      }
    }
  },
  {
    path: 'antest',
    component: AntestComponent,
    data: {
      meta: {
        title: '地產成交, 買樓, 租屋 | 美聯物業',
        description: '美聯物業提供香港一二手買賣及租賃住宅單位之地產代理服務。本網站提供網上搵樓、最新及歷史成交記錄、各區屋苑物業資訊、新盤推介、樓市分析、地產新聞及網上放盤服務。',
        'og:title': '地產成交, 買樓, 租屋 | 美聯物業'
      }
    }
  },
  {
    path: 'en',
    //component: AntestComponent,
    children: [
      {
        path: 'antest',
        component: AntestComponent
      },
      {
        path: 'antest/:estid',
        component: AntestComponent
      }
    ],
    data: {
      meta: {
        title: 'Midland Realty',
        description: 'Midland Realty is a Hong Kong real estate agency that provides services for the sale and rent of residential properties.  This website provides services including property search, recent transaction statistics, historical prices, estate information, new property recommendations, property market analysis, market news, and listing your property for sale/rent.',
        'og:title': 'Midland Realty',
        'og:type': 'website',
        'og:url': 'http://en.midland.com.hk',
        'og:image': 'http://resrc.midland.com.hk/site-common/images/midland-realty-logo.png'
      }
    }
  },
  {
    path: 'tc',
    //component: AntestComponent,
    children: [
      {
        path: 'antest/:estid',
        component: AntestComponent
      },
      {
        path: 'antest',
        component: AntestComponent
      }
    ],
    data: {
      meta: {
        title: '地產成交, 買樓, 租屋 | 美聯物業',
        description: '美聯物業提供香港一二手買賣及租賃住宅單位之地產代理服務。本網站提供網上搵樓、最新及歷史成交記錄、各區屋苑物業資訊、新盤推介、樓市分析、地產新聞及網上放盤服務。',
        'og:title': '地產成交, 買樓, 租屋 | 美聯物業',
        'og:type': 'website',
        'og:url': 'http://www.midland.com.hk',
        'og:image': 'http://resrc.midland.com.hk/site-common/images/midland-realty-logo.png'
      }
    }
  },
  {
    path: 'sc',
    //component: AntestComponent,
    children: [
      {
        path: 'antest',
        component: AntestComponent
      }
    ],
    data: {
      meta: {
        title: 'Title - 角度测试',
        description: '角度 - 测试 1 2 3',
        'og:title': '地产成交, 买楼, 租屋 | 美联物业',
        'og:type': 'website',
        'og:url': 'http://www.midland.com.hk',
        'og:image': 'http://resrc.midland.com.hk/site-common/images/midland-realty-logo.png'
      }
    }
  },
  { path: '', loadChildren: 'app/home/home.module', pathMatch: 'full' }
];

export default RouterModule.forRoot(routes);
