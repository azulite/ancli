import { Component, OnInit, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Params, Router } from '@angular/router';
// import { FilmListService } from '../film-list.service';
import { Film } from '../model/film'
// import 'rxjs/add/observable/throw';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';

@Component({
    selector: 'app-film-list',
    templateUrl: './film-list.component.html',
    styleUrls: ['./film-list.component.css']
})

export class FilmListComponent implements OnInit {

    films: Film[];

    constructor( @Inject('filmList') private filmList) { }

    ngOnInit() {
        // Load comments
        this.loadFilms();
    }

    loadFilms() {
        // Get all comments
        this.filmList.getFilmList()
            .subscribe(
            films => this.films = films, // Bind to view
            err => {
                // Log errors if any
                console.log(err);
            });
    }
}
