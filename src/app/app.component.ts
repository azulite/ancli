import { Component, Inject, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { FilmListService } from './film-list.service';
import { MetaService } from 'ng2-meta';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = '-- || --- |';
  param = {value: 'world'};

  // private translate: TranslateService;

  // constructor(private filmList: FilmListService) {}
  constructor(
    @Inject('metaService') private metaService,
    @Inject('filmList') private filmList,
    @Inject('estateList') private estateList,
    private translate: TranslateService,
    private route: Router,
    private actroute: ActivatedRoute
  ) {
    console.log(this.route.url);
    translate.setDefaultLang('zh');
    translate.use('zh');
  }

  ngOnInit() {
  }

  switchEn() {
    this.translate.use('en');
  }

  switchChi(translate: TranslateService) {
    this.translate.use('zh');
  }

}
