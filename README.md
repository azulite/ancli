<h3>Dependency Injection</h3>
<i>app.module.ts</i>
providers: [{provide: 'filmList', useClass: FilmListService}]

<i>app.component.ts</i>
constructor(@Inject('filmList') private filmList) {}
<hr />

<h3>Child Component</h3>
Refer to <i>app.component.html</i>
<hr />

<h3>Pass data to child component</h3>
Refer to app.component.html and print-btn.component.ts
<hr />

<h3>Data looping</h3>
<ul *ngFor="let cat of filmList.filmcatlist">
  <li>
    <h5>{{cat.title}}</h5>
    <p>{{cat.name}}</p>
  </li>
</ul>
<hr />

<h3>Observable</h3>
Refer to film-list.component.ts, film-list.service.ts, model/film.ts

<h3>Material icons | md-icon</h3>
https://material.io/icons/#ic_accessibility

<h3>Angular flex layout</h3>

<h3>TODO</h3>
Title service, meta tags


# Ancli

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-beta.31.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
